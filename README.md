package com.example.mkrzysiak.controlarduino;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;
import android.bluetooth.BluetoothAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final UUID BTMODULEUUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");

    BluetoothAdapter blueAdapter = BluetoothAdapter.getDefaultAdapter();
    BluetoothDevice blueDevice;
    BluetoothSocket blueSocket;

    private OutputStream outStream;
    private InputStream inStream;
    Button btn_show_auto;
    Button re2;
    EditText edt_text;
    Button btn_blue_on_off;
    LinearLayout panel_wyjscia;
    Switch btn_re1, btn_re2, btn_re3, btn_re4, btn_re5, btn_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_show_auto = (Button) findViewById(R.id.btn_show_auto);
        btn_blue_on_off = (Button) findViewById(R.id.btn_blue);
        panel_wyjscia = (LinearLayout) findViewById(R.id.panel_wyjscia);
        edt_text = (EditText) findViewById(R.id.edt_auto);
        btn_re1 = (Switch) findViewById(R.id.btn_re1);
        btn_re2 = (Switch) findViewById(R.id.btn_re2);
        btn_re3 = (Switch) findViewById(R.id.btn_re3);
        btn_re4 = (Switch) findViewById(R.id.btn_re4);
        btn_re5 = (Switch) findViewById(R.id.btn_re5);
        btn_re1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String re = (isChecked) ? "ON" : "OF";
                String msg = buttonView.getTag().toString() + " " + re;

                try {
                    outStream.write(msg.getBytes());
                } catch (IOException e) {

                }
            }
        });

        btn_re2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String re = (isChecked) ? "ON" : "OF";
                String msg = buttonView.getTag().toString() + " " + re;

                try {
                    outStream.write(msg.getBytes());
                } catch (IOException e) {

                }
            }
        });

        btn_re3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String re = (isChecked) ? "ON" : "OF";
                String msg = buttonView.getTag().toString() + " " + re;

                try {
                    outStream.write(msg.getBytes());
                } catch (IOException e) {

                }
            }
        });

        btn_re4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String re = (isChecked) ? "ON" : "OF";
                String msg = buttonView.getTag().toString() + " " + re;

                try {
                    outStream.write(msg.getBytes());
                } catch (IOException e) {

                }
            }
        });

        btn_re5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) { String re = (isChecked) ? "ON" : "OF";
                String msg = buttonView.getTag().toString() + " " + re;

                try {
                    outStream.write(msg.getBytes());
                } catch (IOException e) {

                }
            }
        });

        if (!blueAdapter.isEnabled()) {
            blueAdapter.enable();
        }
    }

    public void btn_blue_on_off(View view) {
        try {
            if (blueSocket == null) {
                polacz_z_hc();
            } else{
                rozlacz();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void rozlacz() {
        try {
            blueDevice = null;
            blueSocket.close();
            blueSocket = null;
            panel_wyjscia.setVisibility(View.INVISIBLE);
            btn_show_auto.setVisibility(View.INVISIBLE);
            btn_blue_on_off.setText("Polącz");
        } catch (Exception e) {

        }
    }

    private void polacz_z_hc() {
        try {

            Set<BluetoothDevice> devices = blueAdapter.getBondedDevices();
            if (devices.size() > 0) {
                for (BluetoothDevice device : devices) {
                    if (device.getName().contains("HC-05")) {
                        blueDevice = device;
                        break;
                    }
                }
                if (blueDevice != null) {
                    blueSocket = blueDevice.createRfcommSocketToServiceRecord(SPP_UUID);
                    blueSocket.connect();
                    outStream = blueSocket.getOutputStream();
                    inStream = blueSocket.getInputStream();
                    panel_wyjscia.setVisibility(View.VISIBLE);
                    btn_show_auto.setVisibility(View.VISIBLE);
                    btn_blue_on_off.setText("Polączonos");
                }
            } else {
                panel_wyjscia.setVisibility(View.INVISIBLE);
                btn_show_auto.setVisibility(View.INVISIBLE);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void btn_blue_send(View view) {
       if (re2.getText().toString().equals("ON")){
           re2.setText("OFF");
       } else{
           re2.setText("ON");
       }

        try {
            outStream.write(re2.getText().toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void btn_blue_command_all(View view) {
        Switch sw = (Switch) view;

        Button re1 = (Button) findViewById(R.id.btn_re1);
        Button re2 = (Button) findViewById(R.id.btn_re2);
        Button re3 = (Button) findViewById(R.id.btn_re3);
        Button re4 = (Button) findViewById(R.id.btn_re4);
        Button re5 = (Button) findViewById(R.id.btn_re5);

        String msg;
        String re = (sw.isChecked()) ? "ON" : "OF";
        msg = sw.getTag().toString() + " " + re;

        try {
            outStream.write(msg.getBytes());
        } catch (IOException e) {

        }

    }

    public void show_auto(View view) {
        Intent form = new Intent(MainActivity.this, ActivityAuto.class);
        startActivity(form);
    }

    public void auto_zapis(View view) {
        try {
            outStream.write(edt_text.getText().toString().getBytes());
            edt_text.clearComposingText();
        } catch (IOException e) {

        }
    }
}
